//requires: subRegion.js, canvasFilter.js, colourClassifer.js and its requirements


//icons is a mapping(jon object) of string colour names to their representing image icons!
function processCanvas(canvas,subRegionWidth,subRegionHeight,icons,colourDictionary,dictionaryColourNameFunction)
{
	var ctx = canvas.getContext("2d");
	if(typeof(dictionaryColourNameFunction) !== 'function')
	{
		dictionaryColourNameFunction = function(colourName) { return colourName };
	}
	

	//Run averaging filter to break image down into sub regions
	var subRegions = averagingFilter(subRegionWidth,subRegionHeight,canvas);

	//convert colour dictionary to a list of samples for use with K nearest Neighbour module
	var coloursAsLabeledSamples = colourDictionaryToSamples(colourDictionary,dictionaryColourNameFunction);

	
	for(var i =0; i < subRegions.length; i++)
	{
		//Classify average R,G,B values of sub regions into english colour names
		var region = subRegions[i];
		region.colour = classifyColour(region.r , region.g, region.b, coloursAsLabeledSamples).label;
		//If the name of the colour has a key in icons, use its value to render representing image on screen.
		if(typeof(icons[region.colour]) !== 'undefined')
		{
			ctx.drawImage(icons[region.colour],region.x,region.y);
		}
	}



}


//black,white,red,green,yellow,green,blue,brown,purple,pink,orange gray
function dictToBasicColourName(colourName)
{
	var colourName = colourName.toLowerCase();

	if(contains('white') || equals('quartz'))
	{
		return "white";
	}
	else if(equals('magenta') || equals('orchid') || equals('violet red'))
	{
		return "pink";
	}
	else if(equals("red") || equals('scarlet') ||contains('red'))
	{
		return "red";
	}

	else if(contains("green"))
	{
		return "green";
	}

	else if(contains("blue") || equals("cyan") || equals("aquamarine") || equals('summer sky'))
	{
		return "blue";
	}	

	else if(equals('yellow'))
	{
		return "yellow";
	}
	else if(equals('black'))
	{
		return "black";
	}
	else if(contains('brown') || contains("brass") || contains('copper') || contains('dark wood') || contains('chocolate') || contains('sienna'))
	{
		return "brown";
	}
	else if(equals('dark orchid') || contains('purple') || contains('maroon') || equals('violet'))
	{
		return "purple";
	}
	else if(contains('grey') )
	{
		return "grey";
	}
	else if(contains('gold'))
	{
		return 'gold';
	}
	else if(contains('orange'))
	{
		return 'orange';
	}
	else
	{
		console.log(colourName + " has slipped through.");
		return colourName;
	}



	function contains(cnt)
	{
		return !(colourName.indexOf(cnt) === -1)
	}

	function equals(cnt)
	{
		return colourName === cnt;
	}
}




/*
Stage I: Dark-cool and light-warm (this covers a larger set of colors than English "black" and "white".)
Stage II: Red
Stage III: Either green or yellow
Stage IV: Both green and yellow
Stage V: Blue
Stage VI: Brown
Stage VII: Purple, pink, orange, or gray
*/
