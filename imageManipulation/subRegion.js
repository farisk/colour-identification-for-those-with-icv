/**
 * @fileOverview Class for holding information on sub regions of images.
 * @author Faris Keenan
 * @version 1.0
 */

/**
 * Creates a new subRegion. This defines a region of an image. Can be used to store
 * the R,G,B values of a region. (e.g. averge r,g,b for an averaging filter)
 * @class
 */
function subRegion(x,y,width,height,r,g,b)
{
	this.x = x;
	this.y = y;

	this.r = r;
	this.g = g;
	this.b = b;

	this.width = width;
	this.height = height;
}

function drawSubRegionList(subRegionList,canvas)
{
	var ctx = canvas.getContext("2d");
	for(var i = 0; i < subRegionList.length;i++)
	{
		var region = subRegionList[i];
        ctx.fillStyle="rgb(" + region.r +"," + region.g + "," + region.b + ")";
        ctx.fillRect(region.x,region.y,region.width,region.height);
	}
}

