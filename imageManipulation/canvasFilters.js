/**
 * Calculates the starting index of a pixel in a Canvas Pixel 
 * Array.
 * @param {Number} x
 * @param {Number} y
 * @param {Number} width
 * @return {Number} startIndexOfPixel
 */
function startIndexOfPixel(x,y,width)
{
    return ((y) * (width*4) ) + ((x) * 4);
}

function getRGBOfPixel(pixels,ctx,canvas,x,y)
{
    var startIndex = startIndexOfPixel(x,y,canvas.width);//Get start index for pixel information in image data

    return [pixels[startIndex],pixels[startIndex+1],pixels[startIndex+2]];//Form and return RGB array of pixel x,y            
}
/**
 * Given a canvas which bounds the image to be manipulated, averages the colour of regions 
 * @function
 * @param {{name: string, red: number, green:number}[]} colourDictionary - List of mappings from RGB codes to english colour names
 * @param {function} [nameConvertFunction] - function takes a String and returns a string. Use if you want some custom behaviour related to the setting of sample label names.If not given label names default to colour names.
 * @return {Sample[]} 
 */
function averagingFilter(subWidth,subHeight,canvas)
{
    var ctx = canvas.getContext("2d");
    //var squareX,squareY = 0;//Coordinates of current sub square. Top-left is 0,0.
    var imageWidth = canvas.width;
    var imageHeight = canvas.height;

    console.log(Math.floor( ((imageWidth/subWidth)*(imageHeight/subHeight)) ));
    var retList = new Array(Math.floor( ((imageWidth/subWidth)*(imageHeight/subHeight)) ));

    var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
    var pixels = imageData.data;
    var squareCount = 0;
    for(var squareY = 0;squareY < imageHeight;squareY = squareY + subHeight )
    {
         
        for(var squareX = 0; squareX < imageWidth;squareX = squareX + subWidth)
        {
            //Find the sum of RGB values of all the pixels in the sub-square,
            //later we use this information to find the mean RGB value of the square.
            var totalRed = 0;
            var totalGreen =0;
            var totalBlue = 0;

            var offsetX = 0;
            var sampleCount = 0;
            for(var focusY = 0;focusY < subHeight; focusY++)
            {
                
                for(var focusX = 0; focusX < subWidth; focusX++)
                {
                    //todo:check if OUT OF BOUNDS
                    if((focusX + squareX) > subWidth)
                    {

                    } 
                    var actualX = focusX + squareX;
                    var actualY = focusY + squareY;

                    
                    var rgbValues = getRGBOfPixel(pixels,ctx,canvas,actualX,actualY);

                    totalRed += rgbValues[0];
                    totalGreen += rgbValues[1];
                    totalBlue += rgbValues[2];

                }
            }
            var areasubRegion = subHeight*subWidth;

            //Round the average r,g,b values down to integers.
            var meanRed = Math.floor(totalRed/areasubRegion);
            var meanGreen = Math.floor(totalGreen/areasubRegion);
            var meanBlue = Math.floor(totalBlue/areasubRegion);


            retList[squareCount] = new subRegion(squareX,squareY,subWidth,subHeight,meanRed,meanGreen,meanBlue);
            squareCount = squareCount + 1;

        }
    }

    return retList;

}