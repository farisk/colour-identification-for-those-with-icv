/*
*	Use for insertion sort. Supply a sorted array as the current list parameter and 
*	a new element to be added as NewMember. Supply function as comparisonFunction which
*	takes two parameters (new element,element in list). The function should return true
*	if the new element should replace the element in question in a list. startPoint is used
*	by the recursive function to determine where to start inserting
*/
function insertMemberSortedList(currentList,newMember,comparisonFunction,startPoint)
{
	if(typeof startPoint === "undefined")
	{
		startPoint = 0;//Point to start going through the list.
					   //This is in place to avoid unnecessary looping when reinserting a new member
	}
	for(var i=startPoint;i < currentList.length;i++)
	{
		var existingMember = currentList[i];

		if(existingMember === undefined)//base case in recursive function
		{//This branch deals with the case where an array contains undefined elements... e.g. when the size of the array has been predetermined on creation
			//Nothing exists at this point in the list. put new member here.
			currentList[i] = newMember;
			return currentList;//Job done, return list
		}
		//if true: the newMember should be higher in the list than the exisitingMember
		else if(comparisonFunction(newMember,existingMember))
		{
			currentList[i] = newMember;

			//Insert the replaced member into correct place in list.
			return insertMemberSortedList(currentList,existingMember,comparisonFunction,i+1);//reinsert the replaced member and return sorted list
			//return insertMemberSortedList(newList,existingMember,comparisonFunction,i+1);
		}	
	}

	currentList.push(newMember);//Gone through all elements in list and the new member is 'smaller' than all elements, so append it to the end of the list.
	return currentList;

}

