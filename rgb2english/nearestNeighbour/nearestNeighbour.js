/*
Requires sorting.js
*/
function setPointDistances(dataSet,sampleToClassify,distanceMetric)
{
	if(typeof(distanceMetric) !== 'function')
	{
		distanceMetric = euclideanDistance;//If distance metric not set default to euclidean distance function
	}

	for(var i = 0; i < dataSet.length;i++)
	{
		var currentData = dataSet[i];
		var distanceToNewSample = distanceMetric(currentData.coordinates,sampleToClassify);//Find distance between new sample and exisiting labeled Data
		currentData.setDistanceToPoint(distanceToNewSample);
	}
}

function findKNearestNeighbours(K,sampleToClassify,dataSet,distanceMetric)
{
	var nearestNeighbours = [];
	if(K <= 0)
	{
		return nearestNeighbours;//Return empty list if someone provides value of 0 or less for K. Prevent from crashing in this case
	}
	for(var i = 0; i < dataSet.length;i++)
	{
		var currentData = dataSet[i];

		//If a distanceMetric function is given, use it to compute distance between sampleToClassify and current element in labeled Data set
		//If that is not the case, assume it has already been set beforehand.
		if(typeof distanceMetric == 'function')
		{
			var distanceToNewSample = distanceMetric(currentData.coordinates,sampleToClassify);//Find distance between new sample and exisiting labeled Data
			currentData.setDistanceToPoint(distanceToNewSample);
		}
		
		if(nearestNeighbours.length < K)
		{
			//Populate initial K elements of nearestNeighbour list with the first samples in dataSet
			nearestNeighbours = insertMemberSortedList(nearestNeighbours,currentData,function(newM,oldM) { return newM.getDistanceToPoint() <= oldM.getDistanceToPoint()  });
		}
		else
		{
			var furthestNearestNeighbour = nearestNeighbours[K-1];//The neighbour furthest away is at the end of the sorted list
			if(distanceToNewSample < furthestNearestNeighbour.getDistanceToPoint())
			{
				//New sample is closer than the furthest nearest neighbour, so replace it with the new sample.
				nearestNeighbours.length = K-1;//Delete last entry from list
				nearestNeighbours = insertMemberSortedList(nearestNeighbours,currentData,function(newM,oldM) { return newM.getDistanceToPoint() <= oldM.getDistanceToPoint()  });
			}
		}
	}

	return nearestNeighbours;
}