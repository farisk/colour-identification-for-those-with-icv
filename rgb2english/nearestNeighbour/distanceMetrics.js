function euclideanDistance(point1,point2)
{
	var numberOfDimensions1 = point1.length
	if(numberOfDimensions1 === point2.length)
	{
		var sum = 0;
		for(var i = 0; i < numberOfDimensions1; i++)
		{
			sum = sum + Math.pow((point1[i] - point2[i]),2);
		}

		return Math.sqrt(sum);
	}
	else
	{
		throw new Error("Dimensions of given points (" + numberOfDimensions1 + " and " + point2.length + ") do not match.");
	}
}