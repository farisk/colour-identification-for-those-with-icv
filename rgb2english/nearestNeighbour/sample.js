function Sample(label,coordinates,distanceToPoint)
{
	this.label = label;
	this.coordinates = coordinates;
	this.distanceToPoint = distanceToPoint;

}

Sample.prototype.numberOfDimensions = function()
{
	return this.coordinates.length;
}

Sample.prototype.getDistanceToPoint = function()
{
	if(typeof(this.distanceToPoint) === 'undefined')
	{
		throw new Error("Distance to refrence point has not been set");
	}
	else
	{
		return this.distanceToPoint;
	}
}

Sample.prototype.setDistanceToPoint= function(newDistance)
{
	this.distanceToPoint = newDistance;
}