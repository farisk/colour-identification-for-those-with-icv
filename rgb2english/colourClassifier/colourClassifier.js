/**
 * @fileOverview Tools for taking a RGB code and classifying it into an appropriate english colour name.
 * @author Faris Keenan
 * @version 1.0
 * Requires sample.js from nearestNeighbour
 */

/**
 * Given a list of colour information, returns a corresponding list of Sample objects with the RGB values being the points values.
 * @function
 * @param {{name: string, red: number, green:number}[]} colourDictionary - List of mappings from RGB codes to english colour names
 * @param {function} [nameConvertFunction] - function takes a String and returns a string. Use if you want some custom behaviour related to the setting of sample label names.If not given label names default to colour names.
 * @return {Sample[]} 
 */
function colourDictionaryToSamples(colourDictionary,nameConvertFunction)
{
	if(typeof(nameConvertFunction) !== 'function')
	{
		nameConvertFunction = function(colourName) { return colourName; }
	}
	var sampleList = new Array(colourDictionary.length);
	for(var i =0; i < colourDictionary.length;i++)
	{
		var colour = colourDictionary[i];
		sampleList[i] = new Sample(nameConvertFunction(colour.name),[colour.red,colour.green,colour.blue]);
	}

	return sampleList;
}


function classifyColour(red,green,blue,colourLabeledData)
{
	var nearestNeighbours = findKNearestNeighbours(1,[red,green,blue],colourLabeledData,euclideanDistance);
	return nearestNeighbours[0];
}