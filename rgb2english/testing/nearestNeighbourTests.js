QUnit.test("Calculate Euclidean Distances Correctly", function(assert)
{
	var expectedResult = 4.47213595499958;
	assert.equal(euclideanDistance([4,2,-1,0],[5,1,2,3]),expectedResult);
});

QUnit.test("Calculate Euclidean Distances Correctly (zero)", function(assert)
{
	var expectedResult = 0;
	assert.equal(euclideanDistance([0,0,0,0],[0,0,0,0]),expectedResult);
});

QUnit.test("Throws Error when given points that differ in Dimensions", function(assert)
{

	assert.throws(function() { euclideanDistance([5,4,3,2,5,5,5],[1,2,3,4,5]) }, new Error("Dimensions of given points (7 and 5) do not match."),"Dimension error thrown!");
});

QUnit.test("Find 4/4 Nearest Neighbour return correct sorted list",function(assert)
{
	var k = 4;
	var sample1 = new Sample("sample 1",[5,6,8,9]);//10.246951
	var sample2 = new Sample("sample 2",[1,7,3,5]);//6.557439
	var sample3 = new Sample("sample 3",[4,1,5,1]);//7.874008
	var sample4 = new Sample("sample 4",[0,0,0,0]);//10.246951

	var dataSet = [sample1,sample2,sample3,sample4];

	var toClassify = [6,8,2,1];

	var expectedList = [sample2,sample3,sample4,sample1];

	var nearestNeighbours = findKNearestNeighbours(k,toClassify,dataSet,euclideanDistance);

	assert.deepEqual( nearestNeighbours,expectedList);
});


QUnit.test("Find 3/4 Nearest Neighbour return correct sorted list",function(assert)
{
	var sample1 = new Sample("sample 1",[5,6,8,9]);//10.246951
	var sample2 = new Sample("sample 2",[1,7,3,5]);//6.557439
	var sample3 = new Sample("sample 3",[4,1,5,1]);//7.874008
	var sample4 = new Sample("sample 4",[0,0,0,0]);//10.246951

	var dataSet = [sample1,sample2,sample3,sample4];

	var toClassify = [6,8,2,1];

	var expectedList = [sample2,sample3,sample1];

	var nearestNeighbours = findKNearestNeighbours(3,toClassify,dataSet,euclideanDistance);

	assert.deepEqual( nearestNeighbours,expectedList);
});

QUnit.test("Find 2/4 Nearest Neighbour return correct sorted list",function(assert)
{
	var k = 2;
	var sample1 = new Sample("sample 1",[5,6,8,9]);//10.246951
	var sample2 = new Sample("sample 2",[1,7,3,5]);//6.557439
	var sample3 = new Sample("sample 3",[4,1,5,1]);//7.874008
	var sample4 = new Sample("sample 4",[0,0,0,0]);//10.246951

	var dataSet = [sample1,sample2,sample3,sample4];

	var toClassify = [6,8,2,1];

	var expectedList = [sample2,sample3];

	var nearestNeighbours = findKNearestNeighbours(k,toClassify,dataSet,euclideanDistance);

	assert.deepEqual( nearestNeighbours,expectedList);
});

QUnit.test("Find 1/4 Nearest Neighbour return correct sorted list",function(assert)
{
	var k = 1;
	var sample1 = new Sample("sample 1",[5,6,8,9]);//10.246951
	var sample2 = new Sample("sample 2",[1,7,3,5]);//6.557439
	var sample3 = new Sample("sample 3",[4,1,5,1]);//7.874008
	var sample4 = new Sample("sample 4",[0,0,0,0]);//10.246951

	var dataSet = [sample1,sample2,sample3,sample4];

	var toClassify = [6,8,2,1];

	var expectedList = [sample2];

	var nearestNeighbours = findKNearestNeighbours(k,toClassify,dataSet,euclideanDistance);

	assert.deepEqual( nearestNeighbours,expectedList);
});

QUnit.test("Find 0/4 Nearest Neighbour return correct sorted list",function(assert)
{
	var k = 0;
	var sample1 = new Sample("sample 1",[5,6,8,9]);//10.246951
	var sample2 = new Sample("sample 2",[1,7,3,5]);//6.557439
	var sample3 = new Sample("sample 3",[4,1,5,1]);//7.874008
	var sample4 = new Sample("sample 4",[0,0,0,0]);//10.246951

	var dataSet = [sample1,sample2,sample3,sample4];

	var toClassify = [6,8,2,1];

	var expectedList = [];

	var nearestNeighbours = findKNearestNeighbours(k,toClassify,dataSet,euclideanDistance);
	console.log(nearestNeighbours);
	assert.deepEqual( nearestNeighbours,expectedList);
});


QUnit.test("Find 3/4 Nearest Neighbour when distance is preset via setPointDistance",function(assert)
{
	var sample1 = new Sample("sample 1",[5,6,8,9]);//10.246951
	var sample2 = new Sample("sample 2",[1,7,3,5]);//6.557439
	var sample3 = new Sample("sample 3",[4,1,5,1]);//7.874008
	var sample4 = new Sample("sample 4",[0,0,0,0]);//10.246951

	var dataSet = [sample1,sample2,sample3,sample4];

	var toClassify = [6,8,2,1];

	var expectedList = [sample2,sample3,sample1];

	setPointDistances(dataSet,toClassify,euclideanDistance);

	var nearestNeighbours = findKNearestNeighbours(3,toClassify,dataSet);

	assert.deepEqual( nearestNeighbours,expectedList);
});

