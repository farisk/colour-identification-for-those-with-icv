QUnit.test("Converts colour dictionary to samples as expected without function param", function(assert)
{
	var colourDict = two4uSmallColourDict;
	var conv2samples = colourDictionaryToSamples(colourDict);
	var testPass = true;
	var message = "Success!"
	if(colourDict.length !== conv2samples.length) { assert.notOk(true,"The length of the sample list does not match the length of the dictionary") }
	for(var i = 0; i < colourDict.length;i++)
	{
		if(colourDict[i].name !== conv2samples[i].label)
		{
			testPass = false;
			message = colourDict[i].name + " does not match " + conv2samples[i].label;
			break;
		}

		var red = conv2samples[i].coordinates[0];
		var green = conv2samples[i].coordinates[1];
		var blue = conv2samples[i].coordinates[2];

		if(!(colourDict[i].red === red && colourDict[i].green === green && colourDict[i].blue === blue))
		{
			testPass = false;
			message = "RGB code mismatch " + conv2samples[i].coordinates + " instead of " + rgbCode;
		}
	}

	assert.ok(testPass,message);
});


QUnit.test("Predicts exact RGB codes as expected",function(assert)
{
	var colourDict = two4uSmallColourDict;
	var conv2samples = colourDictionaryToSamples(colourDict);
	var testPassed = true;
	var message = "success!"
	for(var i = 0;i < colourDict.length;i++)
	{
		var nearestNeighbour = classifyColour(colourDict[i].red ,colourDict[i].green,colourDict[i].blue,conv2samples);

		if(nearestNeighbour.label !== colourDict[i].name)
		{
			testPassed = false;
			message = "Classed as " + nearestNeighbour.label  + " should be" + colourDict[i].name;
			break;
		}
	}

	assert.ok(testPassed,message);

});